// Inicia uma contagem regressiva e redireciona para outra página ao final
function feedbackTimeout(time, page) {
  // feedbackTimeout(Duração da contagem regressiva, página carregada ao fim da contagem)  
  var myVar = setInterval(function() {
    if (time >= 0) {
      // ATENÇÃO : Para o resultado ser exibido a página DEVE ter um elemento de texto com o ID "countdown"
      document.getElementById("countdown").innerHTML = time; // Mostra o tempo restante na página.
    
    } else {
      window.clearInterval(myVar); // Encerra a contagem
      gotoPage(page); // Redireciona para a próxima página

    }

    time--;
  }, 1000);
  
  
}



// Redireciona para a página informada na chamada da função e apaga a atual do histórico
function gotoPage(text) {
    window.location.replace(text);
}